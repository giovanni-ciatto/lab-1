package it.unibo.sd1819.lab1;

public abstract class ActiveObject {
	private final Thread thread = new Thread(this::run);
	private boolean running = true;

	private void run() {
		try {
			onBegin();
			while (running) {
				loop();
			}
		} catch (Exception e) {
			onUncaughtException(e);
		} finally {
			onEnd();
		}
	}

	public abstract void loop() throws Exception;

	public abstract void onBegin() throws Exception;

	public abstract void onEnd();

	public void onUncaughtException(Exception e) {
		e.printStackTrace();
	}

	public void start() {
		thread.start();
	}

	public void stop() {
		running = false;
		thread.interrupt();
	}

	public void await() {
		try {
			thread.join();
		} catch (InterruptedException e) {
			// Do nothing
		}
	}
}
